from __future__ import division

n = 600851475143

def problem3():
	def PrimeFactor(n):
	    m = n
	    while n%2==0:
	        n = n//2
	    if n == 1:         # check if only 2 is largest Prime Factor 
	        return 2
	    i = 3
	    sqrt = int(m**(0.5))  # loop till square root of number
	    last = 0              # to store last prime Factor i.e. Largest Prime Factor
	    while i <= sqrt :
	        while n%i == 0:   
	            n = n//i       # reduce the number by dividing it by it's Prime Factor
	            last = i
	        i+=2
	    if n > last:            # the remaining number(n) is also Factor of number 
	        return n
	    else:
	        return last

	print PrimeFactor(n)

def problem7():
	primes = [2]
	i = 1
	j = 1
	while i < 10001:
		flag = True
		while flag:
			j +=1 
			for p in primes:
				if j%p:
					flag = False
					break
			

def problem11():
	#!/bin/python
	import sys
	from operator import mul

	grid = []
	for grid_i in xrange(20):
	    grid_temp = map(int,raw_input().strip().split(' '))
	    grid.append(grid_temp)

	#Rows:
	rowprods = []
	rowmax = max([reduce(mul, grid[row_i][i:i+4], 1) for i in xrange(16) for row_i in xrange(20)])
	colmax = max([grid[i][col_i]*grid[i+1][col_i]*grid[i+2][col_i]*grid[i+3][col_i] for i in xrange(16) for col_i in xrange(20)])

	posmaindiagmax = max([grid[i][i]*grid[i+1][i+1]*grid[i+2][i+2]*grid[i+3][i+3] for i in xrange(17)])
	negmaindiagmax = max([grid[19-i][i]*grid[19-i-1][20-i-1]*grid[19-i-2][i+2]*grid[19-i-3][i+3] for i in xrange(17)])

	#Positive upper diagonal
	for j in xrange(15):
	    print "\n"
	    for k in xrange(17-j):
	        #print j+k, 1+j+k, j+3+k, 1+j+3+k
	        print grid[j+k][1+j+k], grid[j+3+k][1+j+3+k]

def jessecookies():
	N, K = raw_input().split()
	N, K = int(N), int(K)
	cookies = [int(c) for c in raw_input().split()]
	cookies = sorted(cookies)

	for n in xrange(int(N)):
	    if min(cookies) >= K:
	        print n
	        break
	    else:
	        a, b = cookies[0], cookies[1]
	        del cookies[:2]
	        cookies.append(a+2*b)
	        #print cookies
	        cookies = sorted(cookies)

	if n == N:
	    print -1 